Origin Master Repo: https://github.com/lanmower/hyper-relay

HyperMC utilizes this server within its infrastructure to serve HTTP based traffic to our Hyper Proxy Service.   

This package relays tcp listeners over hyperswarm, using the createServer/connect noise encrypted channel.

The cli tools are symetric topics, but one can
easily spawn a client programatically that uses
a public key instead of a topic, to make your
relayed process publicly available.

# Installation
npm install --save hyper-relay -g

## TCP Relay

### remotely
### relay from a hyper-relay client to a local server
hyper-tcp-relay-server <topicname> <portnumber>

### locally
### relay from a local server to a hyper-relay server
hyper-tcp-relay-client <topicname> <portnumber>

