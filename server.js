const crypto = require("hypercore-crypto");
const node = require('./relay.js')();
const b32 = require("hi-base32");
const key = crypto.keyPair(crypto.data(Buffer.from(process.argv[3])));
const port = process.argv[2] || 80;
console.log(b32.encode(key.publicKey).replace('====', '').toLowerCase())
node.serve(key, "localhost", port)
